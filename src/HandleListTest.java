
/**
 * On my honor:
 * - I have not used source code obtained from another student,
 * or any other unauthorized source, either modified or
 * unmodified.
 * 
 * - All source code and documentation used in my program is
 * either my original work, or was derived by me from the
 * source code published in the textbook for this course.
 *
 * - I have not discussed coding details about this project with
 * anyone other than my partner (in the case of a joint
 * submission), instructor, ACM/UPE tutors or the TAs assigned
 * to this course. I understand that I may discuss the concepts
 * of this program with other students, and that another student
 * may help me debug my program so long as neither of us writes
 * anything during the discussion or modifies any computer file
 * during the discussion. I have violated neither the spirit nor
 * letter of this restriction.
 */

import student.TestCase;

/**
 * Tests the methods in HandleList Class
 * 
 * @author Syed Muhammad Farhan (syedfarhan)
 * @author Megan Hicks (Hmegan)
 * @version 09/10/2020
 */
public class HandleListTest extends TestCase {

    /**
     * Single method to test HandleList methods that may not be tested in other
     * test classes
     */
    public void testEverything() {
        HandleList list = new HandleList(10);

        assertNull(list.pop(0));

        Handle h1 = new Handle(4, 0, false);
        Handle h2 = new Handle(4, 4, false);
        Handle h3 = new Handle(4, 8, false);

        list.add(h1, 2);
        list.add(h2, 2);
        list.add(h3, 2);

        assertEquals(h1, list.pop(2));
        assertEquals(h2, list.pop(2));
        assertEquals(h3, list.pop(2));

    }


    /**
     * Additional testing per webCat
     * testing conditions under add method
     */
    // ToDo this doesn't do what I want it to do: trying to make h3 go to
    // list[2]
    // for lines 76-78
    public void testAdd() {
        HandleList list = new HandleList(4);
        Handle h1 = new Handle(4, 0, false);
        Handle h2 = new Handle(4, 64, false);
        Handle h3 = new Handle(4, 32, false);

        list.add(h1, 1);
        list.add(h2, 1);
        list.add(h3, 1);

        assertEquals(h1, list.pop(1));

        assertEquals(h3, list.pop(1));
        assertEquals(h2, list.pop(1));
    }

    /**
     * Testing if remove works correctly
     */
    public void testRemove() {
        HandleList list = new HandleList(4);
        Handle h1 = new Handle(4, 0, true);
        Handle h2 = new Handle(4, 64, true);
        Handle h3 = new Handle(4, 32, true);

        list.add(h1, 1);
        list.add(h2, 1);
        list.add(h3, 1);
        

        assertNotNull(list.gethead(1));
        list.remove(1, h2);
        list.remove(1, h3);
        list.remove(1, h1);
        
        assertNull(list.gethead(1));

    }

}
