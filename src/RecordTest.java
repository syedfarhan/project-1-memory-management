
/**
 * On my honor:
 * - I have not used source code obtained from another student,
 * or any other unauthorized source, either modified or
 * unmodified.
 * 
 * - All source code and documentation used in my program is
 * either my original work, or was derived by me from the
 * source code published in the textbook for this course.
 *
 * - I have not discussed coding details about this project with
 * anyone other than my partner (in the case of a joint
 * submission), instructor, ACM/UPE tutors or the TAs assigned
 * to this course. I understand that I may discuss the concepts
 * of this program with other students, and that another student
 * may help me debug my program so long as neither of us writes
 * anything during the discussion or modifies any computer file
 * during the discussion. I have violated neither the spirit nor
 * letter of this restriction.
 */

import static org.junit.Assert.assertNotEquals;
import student.TestCase;

/**
 * tests the methods in Record class
 * 
 * @author Syed Muhammad Farhan (syedfarhan)
 * @author Megan Hicks (Hmegan)
 * @version 09/10/2020
 * 
 */

public class RecordTest extends TestCase {
    private Record r;

    /**
     * setUp for test cases
     */
    public void setUp() {
        r = new Record("some name", "<SEP>some field<SEP>some value");
    }


    /**
     * test add and delete methods
     */
    public void testAddDelete() {
        assertNotNull(r);

        byte[] data = r.getData();

        r.updateAdd("another filed", "another value");

        assertNotEquals(data, r.getData());

        assertTrue(r.updateDelete("another filed"));

        assertEquals(new String(data), r.getDataString());

        System.out.println((r.getDataString()));

        r.updateAdd("another filed", "another value");
        assertEquals("some name<SEP>some field<SEP>some value<SEP>another"
            + " filed<SEP>another value", r.getDataString());
        System.out.println(r.getDataString());
        assertFalse(r.updateDelete("somevalue"));

        r.updateAdd("some field", "some new value");
        assertEquals("some name<SEP>some field<SEP>some new value<SEP>another "
            + "filed<SEP>another value", new String(r.getData()));

        Record r1 = new Record("myName");
        r1.updateAdd("myfield", "myvalue");
        assertEquals("myName<SEP>myfield<SEP>myvalue", new String(r1
            .getData()));

        Record r2 = new Record("newName");
        assertFalse(r2.updateDelete("nonexistentField"));

    }


    /**
     * 
     */
    public void testAddExtract() {
        String str = "some name" + "<SEP>some field<SEP>some value";
        Record r1 = new Record(str.getBytes());
        assertEquals(str, new String(r1.getData()));

        Record r2 = new Record(("some name").getBytes());

        assertEquals("some name", r2.getDataString());
        assertEquals(r2.getDataString(), new String(r2.getData()));

    }
}
