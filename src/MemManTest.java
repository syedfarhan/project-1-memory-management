
/**
 * On my honor:
 * - I have not used source code obtained from another student,
 * or any other unauthorized source, either modified or
 * unmodified.
 * 
 * - All source code and documentation used in my program is
 * either my original work, or was derived by me from the
 * source code published in the textbook for this course.
 *
 * - I have not discussed coding details about this project with
 * anyone other than my partner (in the case of a joint
 * submission), instructor, ACM/UPE tutors or the TAs assigned
 * to this course. I understand that I may discuss the concepts
 * of this program with other students, and that another student
 * may help me debug my program so long as neither of us writes
 * anything during the discussion or modifies any computer file
 * during the discussion. I have violated neither the spirit nor
 * letter of this restriction.
 */

import student.TestCase;

/**
 * Tests all the methods in MemMan
 * 
 * @author Syed Muhammad Farhan (syedfarhan)
 * @author Megan Hicks (Hmegan)
 * @version 09/10/2020
 */
public class MemManTest extends TestCase {
    /**
     * Sets up the tests that follow. In general, used for initialization
     */
    public void setUp() {
        // Nothing Here
    }


    /**
     * Get code coverage of the class declaration.
     */
    public void testRInit() {
        MemMan manager = new MemMan();
        assertNotNull(manager);
        MemMan.main(null);
    }


    /**
     * test case : incorrect number of inputs
     */
    public void testNoparameters() {
        // Testing for no other arguments
        String[] args = new String[2];
        MemMan.main(args);
        assertEquals(true, systemOut().getHistory().contains("Error"));
        systemOut().clearHistory();
        Parser.delete();
        OperationControl.delete();
    }


    /**
     * test case: file does not exist
     */
    public void testFileNotExist() {
        // Testing for file not found
        String[] args = new String[2];
        args = new String[3];
        args[0] = "100";
        args[1] = "100";
        args[2] = "somefilethatdoesntexist.txt";
        MemMan.main(args);
        assertEquals(true, systemOut().getHistory().contains("Error"));
        systemOut().clearHistory();
        Parser.delete();
        OperationControl.delete();
    }


    /**
     * testcase bad input file
     */
    public void testBadInputFile() {
        String[] args = new String[3];
        args[0] = "4";
        args[1] = "16";
        args[2] = "badinput.txt";
        MemMan.main(args);
        assertEquals(true, systemOut().getHistory().contains("Error"));
        systemOut().clearHistory();
        Parser.delete();
        OperationControl.delete();
    }


    /**
     * hashsize or mem size not being an integer
     */
    public void testNaNSize() {
        String[] args = new String[3];
        args[1] = "100";
        args[2] = "Project1_sampleInput.txt";
        args[0] = "not a number";
        MemMan.main(args);
        assertEquals(true, systemOut().getHistory().contains("Error"));
        systemOut().clearHistory();
        Parser.delete();
        OperationControl.delete();
        args[2] = "Project1_sampleInput.txt";
        args[0] = "100";
        args[1] = "not a number";
        MemMan.main(args);
        assertEquals(true, systemOut().getHistory().contains("Error"));
        systemOut().clearHistory();
        Parser.delete();
        OperationControl.delete();
    }


    /**
     * Sample inputtest
     */
    public void testSampleInput() {
        String[] args = new String[3];
        args[1] = "3";
        args[0] = "16";
        args[2] = "Project1_sampleInput.txt";
        systemOut().clearHistory();
        MemMan.main(args);
        assertFalse(systemOut().getHistory().contains("Error"));
        systemOut().clearHistory();
        Parser.delete();
        OperationControl.delete();
    }
}
