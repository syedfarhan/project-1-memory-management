
/**
 * On my honor:
 * - I have not used source code obtained from another student,
 * or any other unauthorized source, either modified or
 * unmodified.
 * 
 * - All source code and documentation used in my program is
 * either my original work, or was derived by me from the
 * source code published in the textbook for this course.
 *
 * - I have not discussed coding details about this project with
 * anyone other than my partner (in the case of a joint
 * submission), instructor, ACM/UPE tutors or the TAs assigned
 * to this course. I understand that I may discuss the concepts
 * of this program with other students, and that another student
 * may help me debug my program so long as neither of us writes
 * anything during the discussion or modifies any computer file
 * during the discussion. I have violated neither the spirit nor
 * letter of this restriction.
 */

import student.TestCase;

/**
 * Testing the operationControl class
 * 
 * @author Syed Muhammad Farhan (syedfarhan)
 * @author Megan Hicks (Hmegan)
 * @version 09/10/2020
 */
public class OperationControlTest extends TestCase {
    /**
     * Testing the initialization code for OperationControl
     */
    public void testRInit() {
        OperationControl o = OperationControl.getInstance(100, 100);
        assertNotNull(o);
        OperationControl another = OperationControl.getInstance(100, 100);
        assertSame(o, another);
    }


    /**
     * tests UpdateAdd
     */
    public void testUpdateAdd() {
        OperationControl.delete();
        OperationControl.getInstance(5, 256);

        systemOut().clearHistory();

        OperationControl.add("something");
        assertEquals("|something| has been added to" + " the Name database.\n",
            systemOut().getHistory());

        systemOut().clearHistory();

        OperationControl.updateAdd("something else", " ", " ");
        assertEquals("|something else| not updated because it"
            + " does not exist in the Name database.\n", systemOut()
                .getHistory());

        systemOut().clearHistory();
        OperationControl.updateAdd("something", "some " + "field",
            "some value");
        assertEquals("Updated Record: |something<SEP>some field"
            + "<SEP>some value|\n", systemOut().getHistory());

        systemOut().clearHistory();
        OperationControl.updateAdd("something", "some other field",
            "some other value");
        assertEquals("Updated Record: |something<SEP>some field<SEP>some va"
            + "lue<SEP>some other field<SEP>some other value|\n", systemOut()
                .getHistory());

        systemOut().clearHistory();
        OperationControl.updateAdd("something", "some field", "som"
            + "e new value");
        assertEquals("Updated Record: |something<SEP>some other field<SEP>so"
            + "me other value<SEP>some field<SEP>some new value|\n", systemOut()
                .getHistory());

        systemOut().clearHistory();
    }


    /**
     * tests updateDelete
     */
    public void testUpdateDelete() {
        OperationControl.delete();
        OperationControl.getInstance(5, 16);
        OperationControl.add("something");
        systemOut().clearHistory();

        OperationControl.updateDelete("some other thing", "some field");
        assertEquals("|some other thing| not updated because it does not exist "
            + "in the Name database.\n", systemOut().getHistory());

        systemOut().clearHistory();
        OperationControl.updateDelete("something", "some field");
        assertEquals("|something| not updated because the field |some field| "
            + "does not exist\n", systemOut().getHistory());

        OperationControl.updateAdd("something", "some field", "some "
            + "new value");

        systemOut().clearHistory();
        OperationControl.updateDelete("something", "some field");
        assertEquals("Updated Record: |something|\n", systemOut().getHistory());

        systemOut().clearHistory();
        OperationControl.updateDelete("something", "some field");
        assertEquals("|something| not updated because the field |some field| "
            + "does not exist\n", systemOut().getHistory());

        OperationControl.updateAdd("something", "some field", "some "
            + "new value");

        systemOut().clearHistory();
        OperationControl.updateDelete("something", "some field");
        assertEquals("Updated Record: |something|\n", systemOut().getHistory());

    }


    /**
     * test inserting a big string
     */
    public void testBigStringInsert() {
        OperationControl.delete();
        OperationControl.getInstance(5, 16);
        systemOut().clearHistory();
        String str = "some very very very big string";
        OperationControl.add(str);

        assertEquals("Memory pool expanded to be 32 bytes.\n|some very very "
            + "very big string|" + " has been added to the Name database.\n",
            systemOut().getHistory());

    }
    
    /**
     * test switch statement for case default
     */
    public void testSwitchDefault() {
        OperationControl o = OperationControl.getInstance(100, 100);
        String[] incorrectString = new String[1];
        incorrectString[0] = "wrong";
        
        assertFalse(o.performCommand(incorrectString));
    }
}
