
/**
 * On my honor:
 * - I have not used source code obtained from another student,
 * or any other unauthorized source, either modified or
 * unmodified.
 * 
 * - All source code and documentation used in my program is
 * either my original work, or was derived by me from the
 * source code published in the textbook for this course.
 *
 * - I have not discussed coding details about this project with
 * anyone other than my partner (in the case of a joint
 * submission), instructor, ACM/UPE tutors or the TAs assigned
 * to this course. I understand that I may discuss the concepts
 * of this program with other students, and that another student
 * may help me debug my program so long as neither of us writes
 * anything during the discussion or modifies any computer file
 * during the discussion. I have violated neither the spirit nor
 * letter of this restriction.
 */

import student.TestCase;

/**
 * Testing the memory Manager
 * 
 * @author Syed Muhammad Farhan (syedfarhan)
 * @author Megan Hicks (Hmegan)
 * @version 09/10/2020
 */
public class MemManagerTest extends TestCase {
    /**
     * testing initialization code
     */

    /**
     * tests RInit method
     */
    public void testRInit() {
        MemManager manager = new MemManager(64);
        assertNotNull(manager);
    }


    /**
     * tests Add and getData
     */
    public void testAddGet() {
        MemManager m = new MemManager(64);
        byte[] t3 = "some very long string".getBytes();
        Handle h1 = m.add("some string here".getBytes());
        Handle h2 = m.add("another string here".getBytes());
        Handle h3 = m.add(t3);
        String s1 = new String(m.getData(h1));
        String s2 = new String(m.getData(h2));
        byte[] b3 = m.getData(h3);
        assertEquals(s1, "some string here");
        assertEquals(s2, "another string here");
        assertEquals(new String(b3), new String(t3));
    }


    /**
     * tests delete and Print
     */
    public void testDeletePrint() {
        MemManager m = new MemManager(64);

        byte[] t1 = "A String".getBytes();
        byte[] t2 = "String A".getBytes();
        byte[] t3 = ("some very very very very" + "very  long string")
            .getBytes();

        systemOut().clearHistory();
        m.dump();
        assertEquals("64: 0 \n", systemOut().getHistory());

        Handle h1 = m.add(t1);
        m.dump();
        m.delete(h1);

        systemOut().clearHistory();
        m.dump();
        assertEquals("64: 0 \n", systemOut().getHistory());

        h1 = m.add(t1);
        Handle h2 = m.add(t2);
        m.dump();
        Handle h3 = m.add(t3);

        m.dump();

        m.delete(h2);
        m.dump();
        m.delete(h1);
        m.dump();
        m.delete(h3);
        m.dump();

    }


    /**
     * tests a large input string
     */
    public void testLargeInput() {
        MemManager m = new MemManager(8);

        byte[] largeInput = "some awkwardly long string".getBytes();
        systemOut().clearHistory();

        m.add(largeInput);

        String expectedOutput = "Memory pool expanded to be 16 bytes.\r\n"
            + "Memory pool expanded to be 32 bytes.\r\n";

        assertEquals(expectedOutput, systemOut().getHistory());
    }


    /**
     * Test the code for merging empty handles
     */
    public void testMerge() {
        MemManager m = new MemManager(8);

        Handle h = new Handle(8, 0, false);
        Handle h1 = new Handle(8, 8, false);

        Handle merged = m.mergeBlocks(h, h1);

        assertEquals(merged.getLength(), 16);
        assertEquals(merged.getLocationStart(), 0);

    }

}
