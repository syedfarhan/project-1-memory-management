
/**
 * On my honor:
 * - I have not used source code obtained from another student,
 * or any other unauthorized source, either modified or
 * unmodified.
 * 
 * - All source code and documentation used in my program is
 * either my original work, or was derived by me from the
 * source code published in the textbook for this course.
 *
 * - I have not discussed coding details about this project with
 * anyone other than my partner (in the case of a joint
 * submission), instructor, ACM/UPE tutors or the TAs assigned
 * to this course. I understand that I may discuss the concepts
 * of this program with other students, and that another student
 * may help me debug my program so long as neither of us writes
 * anything during the discussion or modifies any computer file
 * during the discussion. I have violated neither the spirit nor
 * letter of this restriction.
 */

import student.TestCase;

/**
 * Code for testing hashtest
 * 
 * @author Megan Hicks (hmegan)
 * @author Syed Farhan (syedfarhan)
 * @version 9/10/2020
 *
 */

public class HashTest extends TestCase {
    /**
     * testing the setup
     */
    public void testRInit() {
        Hash n = new Hash(5000);
        assertNotNull(n);

    }


    /**
     * randomly adding elements and seeing if the hash function works as
     * expected
     */
    public void testRandomWalk() {
        Hash hashTable = new Hash(4);
        Handle h = new Handle(1, 2, true);
        hashTable.add("abra", h);
        assertEquals(1, hashTable.search("abra"));
        assertEquals("abra", hashTable.getKey(1));
        Handle hh = hashTable.getHandle(1);
        assertEquals(h.getLength(), hh.getLength());
        assertEquals(h.getLocationStart(), hh.getLocationStart());

        assertEquals(true, hashTable.delete("abra"));
        assertEquals(false, hashTable.delete("abra"));
        hashTable.print();
        assertFuzzyEquals("Total records: 0", systemOut().getHistory());
        systemOut().clearHistory();

        hashTable.add("somethingelse", null);
        hashTable.delete("somethingelse");

        hashTable.add("asdf", null);
        assertEquals(1, hashTable.search("asdf"));
        hashTable.add("abra", null);
        hashTable.print();
        assertEquals("|asdf| 1\n|abra| 2\n" + "Total records: 2\n", systemOut()
            .getHistory());
        systemOut().clearHistory();

        hashTable.add("dirty", null);
        systemOut().clearHistory();
        hashTable.print();
        assertFuzzyEquals("|asdf| 1\n|abra| 2\n|dirty| 5\n Total records: 3\n",
            systemOut().getHistory());
    }

}
